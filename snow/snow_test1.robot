*** Settings ***
Library           snowflake.connector
Library           OperatingSystem
Library           BuiltIn
Library           DatabaseLibrary

*** Variables ***
${account_url}    av29276.ap-southeast-1
${user}           GSBL
${password}       Vanajarao10
${database}       SNOW_TEST

*** Test Cases ***
Connect to Snowflake
    ${conn}=    snowflake.connector.connect
    ...    account=${account_url}
    ...    user=${user}
    ...    password=${password}
    ...    database=${database}
    Set Suite Variable    ${conn}

Run SQL Query in Snowflake
    ${conn}=    Get Variable Value    ${conn}
    ${query}=   Set Variable   SELECT * FROM siva
    ${result}=  Call Method    ${conn}    execute    ${query}
    @{result_data}=  Call Method    ${result}    fetchall
    Log Many    ${result_data}

*** Settings ***
Library    snowflake.connector
Library    DatabaseLibrary
Library    os
Library    OperatingSystem
Library    ExcelLibrary
Library    Collections
Library    CSVLibrary
Library     OracleDBLibrary
Library     OracleDB



Force Tags  Count

*** Variables ***
${user}        GSBL
${password}    Vanajarao10
${account}     av29276.ap-southeast-1
${database}    SNOW_TEST
${warehouse}    COMPUTE_WH
${schema}    SNOW_TEST

*** Test Cases ***
Connect to Snowflake
    [Documentation]    Connect to a Snowflake database
    ${connection} =    Connect To Snowflake    ${user}    ${password}    ${account}    ${database}    ${warehouse}    ${schema}
    Log    Connection to Snowflake established: ${connection}

Total_Count
    [Tags]  Count
    ${results} =  DatabaseLibrary.Execute Sql String    select count(*) from gsbl    sansTran=True
    Log    ${results}
    Create File    Count.csv    ${results}


*** Keywords ***
Connect To Snowflake
    [Arguments]    ${user}    ${password}    ${account}    ${database}    ${warehouse}    ${schema}
    ${connection} =    Connect    user=${user}    password=${password}    account=${account}    database=${database}    warehouse=${warehouse}    schema=${schema}
    Run Keyword If    '${connection}' == 'None'    Fail    Connection to Snowflake failed: user=${user}, database=${database}, schema=${schema}
    Log    Connection to Snowflake established: ${connection}
    [Return]    ${connection}



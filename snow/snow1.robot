*** Settings ***
Library    snowflake.connector
Library    DatabaseLibrary

*** Variables ***
${user}        GSBL
${password}    Vanajarao10
${account}     av29276.ap-southeast-1
${database}    SNOW_TEST
${warehouse}    SNOW_TEST
${schema}    SNOW_TEST
${connection}    ${EMPTY}
${commitTransaction}    True  # Set to True for transaction commit, or False for rollback

*** Test Cases ***
Connect to Snowflake
    ${connection} =    Connect To Snowflake
    Log    Connection to Snowflake established: ${connection}

Run SQL Query in Snowflake
    Connect To Snowflake
    ${sqlString} =    Set Variable    SELECT * FROM gsbl
    Run Snowflake SQL Query    ${sqlString}    ${commitTransaction}

*** Keywords ***
Connect To Snowflake
    [Documentation]    Connect to a Snowflake database
    ${connection} =    Connect    user=${user}    password=${password}    account=${account}    database=${database}    warehouse=${warehouse}    schema=${schema}
    Run Keyword If    '${connection}' == 'None'
    ...    Fail    Connection to Snowflake failed: user=${user}, database=${database}, schema=${schema}
    [Return]    ${connection}

Run Snowflake SQL Query
    [Arguments]    ${sqlString}    ${commitTransaction}
    ${result} =    Execute Snowflake Query    ${connection}    ${sqlString}    ${commitTransaction}
    Log    Query results: ${result}

Execute Snowflake Query
    [Arguments]    ${connection}    ${sqlString}    ${commitTransaction}
    ${result} =    Run Keyword If    ${commitTransaction} == True
    ...    query    ${connection}    ${sqlString}
    ...    ELSE
    ...    query    ${connection}    ${sqlString}
    Run Keyword If    ${commitTransaction} == True
    ...    Commit Transaction    ${connection}
    ...    ELSE
    ...    Rollback Transaction    ${connection}
    [Return]    ${result}


import yaml
import snowflake.connector

def load_config(config_file):
    with open(config_file, 'r') as file:
        return yaml.safe_load(file)

def connect_snowflake(config):
    try:
        conn = snowflake.connector.connect(
            account=config['snowflake']['account'],
            user=config['snowflake']['user'], #Or use authenticator for sso.
            authenticator = "externalbrowser", #for sso.
            warehouse=config['snowflake']['warehouse'],
            database=config['snowflake']['database'],
            schema=config['snowflake']['schema']
        )
        return conn
    except Exception as e:
        print(f"Error connecting to Snowflake: {e}")
        return None

def execute_query(conn, query):
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        results = cursor.fetchall()
        cursor.close()
        return results
    except Exception as e:
        print(f"Error executing query: {e}")
        return None

def generate_duplicate_query(table_name, column_name):
    return f"SELECT {column_name}, COUNT(*) FROM {table_name} GROUP BY {column_name} HAVING COUNT(*) > 1;"

def generate_null_query(table_name, column_name):
    return f"SELECT COUNT(*) FROM {table_name} WHERE {column_name} IS NULL;"

def get_column_names(config):
    return config['tables']
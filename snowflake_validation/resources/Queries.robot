*** Settings ***
Library    Collections
Library    OperatingSystem
Library    BuiltIn

*** Keywords ***
Generate SQL Query
    [Arguments]    ${test_type}    ${database}    ${schema}    ${table}
    ${columns}    Get Table Columns    ${test_type}    ${table}
    ${query}    Run Keyword If    '${test_type}' == 'null'
    ...    Generate Null Query    ${database}    ${schema}    ${table}    ${columns}
    ...    ELSE
    ...    Generate Duplicate Query    ${database}    ${schema}    ${table}    ${columns}
    RETURN    ${query}

Generate Null Query
    [Arguments]    ${database}    ${schema}    ${table}    ${columns}
    ${conditions}    Create List
    FOR    ${column}    IN    @{columns}
        Append To List    ${conditions}    ${column} IS NULL
    END
    ${where_clause}    Evaluate    ' OR '.join(${conditions})
    ${query}    Set Variable    SELECT * FROM ${database}.${schema}.${table} WHERE ${where_clause};
    RETURN    ${query}

Generate Duplicate Query
    [Arguments]    ${database}    ${schema}    ${table}    ${columns}
    ${column_list}    Evaluate    ', '.join(${columns})
    ${query}    Set Variable    SELECT ${column_list}, COUNT(*) FROM ${database}.${schema}.${table} GROUP BY ${column_list} HAVING COUNT(*) > 1;
    RETURN    ${query}

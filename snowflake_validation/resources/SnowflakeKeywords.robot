*** Settings ***
Library    snowflake.connector
Library    yaml
Library    OperatingSystem
Library    Collections
Library    BuiltIn

*** Variables ***
${EXECDIR}    ${CURDIR}
${RESULT_FILE}    ${EXECDIR}/reports/validation_results.csv
${SNOWFLAKE_CONN}    None

*** Keywords ***

Connect To Snowflake
    [Arguments]    ${USER}    ${PASSWORD}    ${ACCOUNT}    ${DATABASE}    ${SCHEMA}    ${ROLE}    ${WAREHOUSE}
    ${conn}    Evaluate    snowflake.connector.connect(user='${USER}', password='${PASSWORD}', account='${ACCOUNT}', database='${DATABASE}', schema='${SCHEMA}', role='${ROLE}', warehouse='${WAREHOUSE}')    modules=snowflake.connector
    Set Global Variable    ${SNOWFLAKE_CONN}    ${conn}

Close Snowflake Connection
    Run Keyword If    '${SNOWFLAKE_CONN}' != 'None'    Call Method    ${SNOWFLAKE_CONN}    close

Load Table Configuration
    [Arguments]    ${test_type}
    ${yaml_file}    Set Variable    ${EXECDIR}/config/${test_type}.yaml
    ${config}    Load YAML File    ${yaml_file}
    RETURN    ${config}

Execute Query
    [Arguments]    ${query}
    ${cursor}    Call Method    ${SNOWFLAKE_CONN}    cursor
    Call Method    ${cursor}    execute    ${query}
    ${results}    Call Method    ${cursor}    fetchall
    RETURN    ${results}

Load YAML File
    [Arguments]    ${file_path}
    ${normalized_path}    Normalize Path    ${file_path}
    File Should Exist    ${normalized_path}
    ${file_content}    Get File    ${normalized_path}
    ${config}    Evaluate    yaml.safe_load('''${file_content}''')    modules=yaml
    RETURN    ${config}

Get Table Columns
    [Arguments]    ${test_type}    ${table}
    ${yaml_file}    Set Variable    ${EXECDIR}/config/${test_type}.yaml
    ${config}    Load YAML File    ${yaml_file}
    ${columns}    Get From Dictionary    ${config}    ${table}    default=[]
    RETURN    ${columns}

Export Results To CSV
    [Arguments]    ${results}    ${file_path}

    # Ensure the directory exists
    ${dir}    Evaluate    os.path.dirname(r"${file_path}")    modules=os
    Run Keyword If    '${dir}' and not os.path.exists('${dir}')    Create Directory    ${dir}

    # Create CSV file and write header
    ${header}    Set Variable    Column1,Column2,Column3  # Modify based on actual columns
    Create File    ${file_path}    ${header}\n

    # Append results to the file
    FOR    ${row}    IN    @{results}
        ${line}    Evaluate    ','.join([str(item) for item in ${row}])
        Append To File    ${file_path}    ${line}\n
    END

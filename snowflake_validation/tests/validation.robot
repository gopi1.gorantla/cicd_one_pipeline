*** Settings ***
Library    Collections
Library    OperatingSystem
Resource   ../resources/SnowflakeKeywords.robot
Resource   ../resources/Queries.robot

Suite Setup      Connect To Snowflake    ${USER}    ${PASSWORD}    ${ACCOUNT}    ${DATABASE}    ${SCHEMA}    ${ROLE}    ${WAREHOUSE}
Suite Teardown   Close Snowflake Connection

*** Variables ***
${EXECDIR}       ${CURDIR}
${RESULT_FILE}   ${EXECDIR}/reports/validation_results.csv
${USER}        GGPRAO
${PASSWORD}    Vanajarao@10
${ACCOUNT}     eo29050.ap-southeast-1
${DATABASE}    gopi
${SCHEMA}      gopi_schema
${ROLE}        ACCOUNTADMIN
${WAREHOUSE}   COMPUTE_WH

*** Test Cases ***
Run Data Validation
    [Documentation]  Validate Snowflake data for ${TEST_TYPE} values
    ${config}    Load Table Configuration    ${TEST_TYPE}

    FOR    ${table}    IN    @{config.keys()}
        ${columns}    Get From Dictionary    ${config}    ${table}

        ${query}    Run Keyword If    '${TEST_TYPE}' == 'null'
        ...    Generate SQL Query    null    ${DATABASE}    ${SCHEMA}    ${table}
        ...    ELSE
        ...    Generate SQL Query    duplicate    ${DATABASE}    ${SCHEMA}    ${table}

        ${results}    Execute Query    ${query}
        Export Results To CSV    ${results}    ${RESULT_FILE}
    END

    File Should Exist    ${RESULT_FILE}

import argparse
import os
from snowflake_validation.src.db_connector import SnowflakeDB
from snowflake_validation.src.query_generator import load_yaml, generate_query
from snowflake_validation.src.file_handler import save_results_to_csv

def generate_and_execute_queries(test_type, database, schema, role, warehouse, yaml_file_path):
    # Establish connection to Snowflake
    db = connect_to_snowflake(database, schema, role, warehouse)

    # Load the table configuration from YAML file
    tables = load_yaml(yaml_file_path)

    # Loop through the tables and execute queries
    for table in tables.keys():
        query = generate_query(test_type, database, schema, table, yaml_file_path)
        results = db.execute_query(query)

        # Save the results to a CSV file
        save_results_to_csv(results)

    # Close the Snowflake connection
    db.close()

def connect_to_snowflake(database, schema, role, warehouse):
    # Ensure role is passed and used in the SnowflakeDB connection
    return SnowflakeDB(database, schema, role, warehouse)

def main():
    # Argument parsing with argparse for user input from command line
    parser = argparse.ArgumentParser()
    parser.add_argument("--database", required=True, help="Database to connect to")
    parser.add_argument("--schema", required=True, help="Schema to query")
    parser.add_argument("--role", required=True, help="Role to use for queries")
    parser.add_argument("--warehouse", required=True, help="Warehouse to use for queries")
    parser.add_argument("--test_type", choices=["null", "duplicate"], required=True, help="Type of validation to run")

    args = parser.parse_args()

    # Dynamically select the YAML file based on the test type (null or duplicate)
    yaml_file_path = os.path.join(os.path.dirname(__file__), "..", "config", f"{args.test_type}.yaml")

    # Ensure the YAML file exists
    if not os.path.isfile(yaml_file_path):
        print(f"Error: {yaml_file_path} does not exist.")
        return

    # Generate and execute queries with the provided arguments
    generate_and_execute_queries(args.test_type, args.database, args.schema, args.role, args.warehouse, yaml_file_path)

if __name__ == "__main__":
    main()

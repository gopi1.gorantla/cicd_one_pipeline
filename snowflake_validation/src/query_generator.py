import yaml
import sys
from robot.api.deco import keyword

@keyword
def load_yaml(file_path):
    """Loads YAML configuration file."""
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)

def generate_query(test_type, database, schema, table, yaml_file_path):
    """Generates SQL query based on test type (null or duplicate)."""
    config = load_yaml(yaml_file_path)
    columns = config.get(table, [])

    if not columns:
        raise ValueError(f"No columns found for table: {table}")

    if test_type == "null":
        column_conditions = ' OR '.join([f"{column} IS NULL" for column in columns])
        query = f"SELECT * FROM {database}.{schema}.{table} WHERE {column_conditions};"

    elif test_type == "duplicate":
        column_list = ', '.join(columns)
        query = f"SELECT {column_list}, COUNT(*) FROM {database}.{schema}.{table} GROUP BY {column_list} HAVING COUNT(*) > 1;"

    return query

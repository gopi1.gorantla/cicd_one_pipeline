import csv
import os

def save_results_to_csv(results, file_path="reports/validation_results.csv"):
    os.makedirs(os.path.dirname(file_path), exist_ok=True)

    with open(file_path, "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(["Column Values", "Count"])
        writer.writerows(results)

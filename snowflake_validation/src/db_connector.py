from robot.api.deco import keyword
import snowflake.connector

class SnowflakeDB:
    def __init__(self, database, schema, role, warehouse):
        self.conn = snowflake.connector.connect(
            user="GGPRAO",
            password="Vanajarao@10",
            account="eo29050.ap-southeast-1",
            database=database,
            schema=schema,
            role=role,
            warehouse=warehouse
        )
        self.cursor = self.conn.cursor()

    @keyword
    def execute_query(self, query):
        """Executes a query and returns the results."""
        self.cursor.execute(query)
        return self.cursor.fetchall()

    @keyword
    def close(self):
        """Closes the connection to Snowflake."""
        self.cursor.close()
        self.conn.close()

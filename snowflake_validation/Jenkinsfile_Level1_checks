pipeline {
    agent any

    parameters {
        string(name: 'DATABASE', defaultValue: 'MY_DB', description: 'Snowflake Database')
        string(name: 'SCHEMA', defaultValue: 'MY_SCHEMA', description: 'Snowflake Schema')
        string(name: 'ROLE', defaultValue: 'MY_ROLE', description: 'Snowflake Role')
        string(name: 'WAREHOUSE', defaultValue: 'MY_WAREHOUSE', description: 'Snowflake Warehouse')
        string(name: 'TABLE_SUFFIX', defaultValue: '', description: 'Optional table suffix')
        choice(name: 'TEST_TYPE', choices: ['null', 'duplicate'], description: 'Choose test type')
    }

    environment {
        GIT_REPO = 'https://gitlab.com/gopi1.gorantla/cicd_one_pipeline.git'
        BRANCH = 'main'
        PYTHON_PATH = 'C:\\Users\\gopip\\AppData\\Local\\Programs\\Python\\Python39'
    }

    stages {
        stage('Clone Repo') {
            steps {
                script {
                    try {
                        git branch: "${BRANCH}", url: "${GIT_REPO}"
                    } catch (Exception e) {
                        error "Git checkout failed: ${e.getMessage()}"
                    }
                }
            }
        }

        stage('Setup Environment') {
            steps {
                script {
                    bat "${PYTHON_PATH}\\python -m venv venv"
                    bat "venv\\Scripts\\activate && pip install --cache-dir .jenkins\\pip_cache -r snowflake_validation/requirements.txt"
                }
            }
        }

        stage('Run Data Validation') {
            steps {
                script {
                    bat """
                    venv\\Scripts\\activate && python -m src.main ^
                    --database ${params.DATABASE} ^
                    --schema ${params.SCHEMA} ^
                    --role ${params.ROLE} ^
                    --warehouse ${params.WAREHOUSE} ^
                    --test_type ${params.TEST_TYPE}
                    """
                }
            }
        }

        stage('Run Robot Tests') {
            steps {
                script {
                    bat """
                    venv\\Scripts\\activate && set PYTHONPATH=%CD%\\snowflake_validation\\keywords && robot -d reports ^
                    --variable DATABASE:${params.DATABASE} ^
                    --variable SCHEMA:${params.SCHEMA} ^
                    --variable ROLE:${params.ROLE} ^
                    --variable WAREHOUSE:${params.WAREHOUSE} ^
                    --variable TEST_TYPE:${params.TEST_TYPE} ^
                    snowflake_validation/tests/validation.robot
                    """
                }
            }
        }
    }

    post {
        always {
            script {
                node {
                    archiveArtifacts artifacts: 'reports/*.log, reports/*.xml, reports/*.html', fingerprint: true
                }
            }
        }
        failure {
            echo "Pipeline failed!"
        }
    }
}

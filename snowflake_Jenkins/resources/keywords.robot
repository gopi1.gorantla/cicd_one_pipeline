*** Keywords ***
Run Validation Script
    [Arguments]    ${database}    ${schema}    ${role}    ${warehouse}    ${suffix}    ${date_type}    ${test_type}
    ${result} =    Run Process    python    -m    src.main
    ...    --database=${database}
    ...    --schema=${schema}
    ...    --role=${role}
    ...    --warehouse=${warehouse}
    ...    --suffix=${suffix}
    ...    --date_type=${date_type}
    ...    --test_type=${test_type}
    Log    ${result.stdout}

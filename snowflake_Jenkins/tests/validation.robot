*** Settings ***
Library    OperatingSystem
Library    Process
Library    BuiltIn

*** Variables ***
${DATABASE}       %{ENV_DATABASE}
${SCHEMA}         %{ENV_SCHEMA}
${ROLE}           %{ENV_ROLE}
${WAREHOUSE}      %{ENV_WAREHOUSE}
${TABLE_SUFFIX}   %{ENV_TABLE_SUFFIX}
${DATE_COLUMN_TYPE}  %{ENV_DATE_COLUMN_TYPE}
${TEST_TYPE}      %{ENV_TEST_TYPE}

*** Test Cases ***
Run Snowflake Data Validation
    ${result}=    Run Process    python    -m    src.main    --database ${DATABASE}    --schema ${SCHEMA}    --role ${ROLE}    --warehouse ${WAREHOUSE}    --suffix ${TABLE_SUFFIX}    --date_type ${DATE_COLUMN_TYPE}    --test_type ${TEST_TYPE}
    Log    ${result.stdout}
    Should Be Equal As Integers    ${result.rc}    0
    File Should Exist    reports/validation_results.csv

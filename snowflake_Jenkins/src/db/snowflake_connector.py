import snowflake.connector

def connect_snowflake(database, schema, role, warehouse):
    try:
        conn = snowflake.connector.connect(
            user="GGPRAO",
            password="Vanajarao@10",
            account="eo29050.ap-southeast-1",
            warehouse=warehouse,
            database=database,
            schema=schema,
            role=role
        )
        print("✅ Connected to Snowflake")
        return conn
    except Exception as e:
        print(f"❌ Snowflake connection failed: {e}")
        exit(1)

# Run a simple query to check if connection works
conn = connect_snowflake("MY_DB", "MY_SCHEMA", "MY_ROLE", "MY_WAREHOUSE")
cursor = conn.cursor()
cursor.execute("SELECT CURRENT_VERSION()")
print("Snowflake Version:", cursor.fetchone()[0])

def generate_query(database, schema, table_name, columns, date_column, date_value, test_type):
    """Generate a SQL query for null values or duplicates based on test type."""

    column_list = ", ".join(columns)  # Join columns for the query

    if test_type == "null":
        return f"""
        SELECT '{table_name}' AS table_name, '{column_list}' AS columns_checked, COUNT(*)
        FROM {database}.{schema}.{table_name}
        WHERE {date_column} = '{date_value}'
        AND ({' OR '.join([f"{col} IS NULL" for col in columns])})
        GROUP BY {column_list}
        """

    elif test_type == "duplicate":
        return f"""
        SELECT {column_list}, COUNT(*)
        FROM {database}.{schema}.{table_name}
        WHERE {date_column} = '{date_value}'
        GROUP BY {column_list}
        HAVING COUNT(*) > 10
        """

    return None  # Return None if the test type is invalid

import argparse
import csv
import os
from src.db.snowflake_connector import get_snowflake_connection
from src.db.query_generator import generate_query
from src.db.yaml_reader import load_yaml

def execute_query(conn, query):
    """Execute a SQL query and return results."""
    cursor = conn.cursor()
    cursor.execute(query)
    results = cursor.fetchall()
    cursor.close()
    return results

def write_to_csv(results, filename):
    """Write query results to a CSV file."""
    os.makedirs("reports", exist_ok=True)  # Ensure reports folder exists
    with open(filename, mode="w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(["Table", "Columns Checked", "Count"])  # Header row
        writer.writerows(results)
    print(f"Results written to {filename}")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--database", required=True)
    parser.add_argument("--schema", required=True)
    parser.add_argument("--role", required=True)
    parser.add_argument("--warehouse", required=True)
    parser.add_argument("--suffix", default="")
    parser.add_argument("--date_type", required=True)
    parser.add_argument("--test_type", required=True)
    args = parser.parse_args()

    conn = get_snowflake_connection()
    yaml_data = load_yaml("snowflake_Jenkins/config/tables.yaml")

    results = []
    for table, columns in yaml_data.items():
        table_name = f"{table}{args.suffix}"  # Handle table suffix
        date_column = "ingestdate" if args.date_type == "ingestdate" else "reportingdate"

        query = generate_query(args.database, args.schema, table_name, columns, date_column, args.date_type, args.test_type)
        if query:
            print(f"Executing query: {query}")  # Debugging log
            query_results = execute_query(conn, query)
            results.extend(query_results)

    conn.close()
    write_to_csv(results, "reports/validation_results.csv")

if __name__ == "__main__":
    main()

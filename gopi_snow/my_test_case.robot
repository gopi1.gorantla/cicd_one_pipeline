*** Settings ***
Library  custom_snowflake.py
Library     OperatingSystem
Library     ExcelLibrary
Library     Collections
Library     CSVLibrary

*** Variables ***
${account_url}    av29276.ap-southeast-1
${user}           GSBL
${password}       Vanajarao10
${database}       SNOW_TEST

*** Test Cases ***
Connect to Snowflake
    [Tags]  Snowflake
    ${conn}=  Connect to Snowflake  ${account_url}  ${user}  ${password}  ${database}
    ${query}=  Set Variable  SELECT count(*) FROM gsbl
    @{result_data}=  Execute Query  ${conn}  ${query}
    Log Many  ${result_data}
    Create File  count.csv  (${result_data})

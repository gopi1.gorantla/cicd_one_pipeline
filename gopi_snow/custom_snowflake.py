# custom_snowflake.py

import snowflake.connector

def connect_to_snowflake(account, user, password, database):
    conn = snowflake.connector.connect(
        account=account,
        user=user,
        password=password,
        database=database
    )
    return conn

def execute_query(conn, query):
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    return result
# snowflake_library.robot

*** Settings ***
Library  custom_snowflake.SnowflakeLibraryWrapper

*** Keywords ***
Connect To Snowflake
    [Arguments]  ${account_url}  ${user}  ${password}  ${database}
    Connect To Snowflake  ${account_url}  ${user}  ${password}  ${database}

Execute SQL Query in Snowflake
    [Arguments]  ${query}
    ${result}=  Execute Query  ${query}
    [Return]  ${result}

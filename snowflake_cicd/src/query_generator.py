def generate_query(table_name, columns, test_type):
    if test_type == "null":
        return f"SELECT COUNT(*) FROM {table_name} WHERE " + " OR ".join([f"{col} IS NULL" for col in columns])
    elif test_type == "duplicate":
        return f"SELECT {', '.join(columns)}, COUNT(*) FROM {table_name} GROUP BY {', '.join(columns)} HAVING COUNT(*) > 1"

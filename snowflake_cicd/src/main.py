import argparse
from src.yaml_reader import load_yaml_config
from src.validator import validate_table
from src.exporter import export_results

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--database', required=True)
    parser.add_argument('--schema', required=True)
    parser.add_argument('--role', required=True)
    parser.add_argument('--warehouse', required=True)
    parser.add_argument('--suffix', default='')
    parser.add_argument('--date_type', required=True)
    parser.add_argument('--test_type', required=True)
    args = parser.parse_args()

    config_file = f"config/{args.test_type}.yaml"
    tables_config = load_yaml_config(config_file)["snowflake_tables"]
    results = []

    for table in tables_config:
        table_name = table["name"] + args.suffix
        columns = table["columns"]
        status = validate_table(table_name, columns, args.test_type)
        results.append((table_name, args.test_type, status))

    export_results(results)

if __name__ == "__main__":
    main()

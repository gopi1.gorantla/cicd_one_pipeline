import csv

def export_results(results, filename="validation_results.csv"):
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Table", "Test Type", "Status"])
        writer.writerows(results)

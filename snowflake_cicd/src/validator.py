from src.snowflake_connector import get_connection
from src.query_generator import generate_query

def validate_table(table_name, columns, test_type):
    conn = get_connection()
    cursor = conn.cursor()
    query = generate_query(table_name, columns, test_type)
    cursor.execute(query)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    return "Pass" if not result else "Fail"

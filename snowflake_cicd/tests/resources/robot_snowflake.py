from src.snowflake_connector import get_connection
from robot.api.deco import keyword

class SnowflakeKeywords:
    def __init__(self):
        self.conn = None

    @keyword("Connect To Snowflake")
    def connect_to_snowflake(self):
        self.conn = get_connection()

    @keyword("Validate Table")
    def validate_table(self, table_name, test_type):
        from src.validator import validate_table
        return validate_table(table_name, test_type)

    @keyword("Disconnect From Snowflake")
    def disconnect_from_snowflake(self):
        if self.conn:
            self.conn.close()

snowflake_keywords = SnowflakeKeywords()

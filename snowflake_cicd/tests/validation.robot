*** Settings ***
Library  Collections
Library  ../src/yaml_reader.py  # Correct path for YAML reader
Library  resources/robot_snowflake.py

Suite Setup  Connect To Snowflake
Suite Teardown  Disconnect From Snowflake

*** Variables ***
${DATABASE}    MY_DB
${SCHEMA}      MY_SCHEMA
${ROLE}        MY_ROLE
${WAREHOUSE}   MY_WAREHOUSE

*** Test Cases ***
Validate Tables for Null
    @{NULL_TABLES}=  Get Tables  ../config/null.yaml  # Updated path
    FOR  ${TABLE}  IN  @{NULL_TABLES}
        ${status_null}=  Validate Table  ${TABLE}  null
        Should Be Equal  ${status_null}  Pass
    END

Validate Tables for Duplicates
    @{DUPLICATE_TABLES}=  Get Tables  ../config/duplicate.yaml  # Updated path
    FOR  ${TABLE}  IN  @{DUPLICATE_TABLES}
        ${status_duplicate}=  Validate Table  ${TABLE}  duplicate
        Should Be Equal  ${status_duplicate}  Pass
    END

*** Keywords ***
Connect To Snowflake
    [Arguments]    ${user}    ${password}    ${account}    ${warehouse}    ${database}    ${schema}
    ${connection} =    Connect    user=${user}    password=${password}    account=${account}    warehouse=${warehouse}    database=${database}    schema=${schema}
    [Return]    ${connection}
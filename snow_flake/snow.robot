*** Settings ***
Library    snowflake.connector
#Library    snow.py
Resource    connection_keyword.robot
Variables    Connection_variables.robot



*** Test Cases ***
Connect to Snowflake
    [Documentation]    Connect to a Snowflake database
    ${connection} =    Connect To Snowflake    ${DB_User}    ${DB_Password}    ${DB_Account}    ${DB_Warehouse}    ${DB_Database}    ${DB_Schema}
    Log    Connection to Snowflake established: ${connection}


*** Settings ***
Library     SeleniumLibrary
Library     DatabaseLibrary
Library     XML
Library     os
Library     OperatingSystem
Library     ExcelLibrary
Library     OracleDBLibrary
Library     OracleDB
Library     Collections
Library     CSVLibrary

Suite Setup  Connect To Database Using Custom Params  cx_Oracle  'INFA_REP','tiger','XE'

Force Tags  Duplicate  Null


*** Variables ***
${Container}  dharani

*** Test Cases ***

Duplicate_Checks
    [Tags]  Duplicate
    ${results}=  query  select VILLAGE,count(*) from bharagav group by VILLAGE having count(*)>1
    Set Test Variable    ${results}
    log  ${results}
    Create File  duplicate.csv  (${results})

Null_Checks
    [Tags]  Null
    ${results}=  query  select * from bharagav where PHONE IS NOT NULL
    Set Test Variable    ${results}
    log  ${results}
    Create File  Null.csv  (${results})









import calendar

# create a calendar object for 2023
cal = calendar.Calendar()

# loop over each month in 2023
for month in range(1, 13):
    # get the month's calendar as a string
    month_str = calendar.month_name[month]
    cal_str = cal.formatmonth(2023, month)

    # print the month and its calendar
    print(f"{month_str} 2023")
    print(cal_str)
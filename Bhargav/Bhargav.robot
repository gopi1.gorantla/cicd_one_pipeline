*** Settings ***
Library     SeleniumLibrary
Library     DatabaseLibrary
Library     XML
Library     os
Library     OperatingSystem
Library     ExcelLibrary
Library     OracleDBLibrary
Library     OracleDB
Library     Collections
Library     CSVLibrary

#Suite Setup  Connect To Database Using Custom Params  cx_Oracle  'INFA_REP','gsbl','XE'
#
#Force Tags  Duplicate  Null


*** Variables ***
${url}  https://www.flipkart.com/
${sai}
*** Test Cases ***
Connect to Flipkart
    [Tags]  Login to Flipkart
    open browser    ${url}    firefox
    sleep   50
    Maximize Browser Window
#    Input Text    //*[@id="email"]    9014255161
#    Input Password    //input[@id="pass"]    Vanajarao@10
#    Click Button    //button[@name="login"]
#    Sleep    10
#    capture page screenshot
#    Close Browser

*** Test Cases ***

#Total_Count
#    [Tags]  Count
#    ${results}=  query  select count(*) from bharagav
#    Set Test Variable    ${results}
#    log  ${results}
#    Create File  Count.csv  (${results})
#
#Duplicate_Checks
#    [Tags]  Duplicate
#    ${results}=  query  select VILLAGE,count(*) from bharagav group by VILLAGE having count(*)>1
#    Set Test Variable    ${results}
#    log  ${results}
#    Create File  duplicate.csv  (${results})
#
#Null_Checks
#    [Tags]  Null
#    ${results}=  query  select * from bharagav where PHONE IS NOT NULL
#    Set Test Variable    ${results}
#    log  ${results}
#    Create File  Null.csv  (${results})
##    Test

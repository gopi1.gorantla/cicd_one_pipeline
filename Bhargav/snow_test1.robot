*** Settings ***
Library  snowflake.connector
Library  OperatingSystem
Library  BuiltIn

*** Variables ***
${account_url}=   av29276.ap-southeast-1
${user}=          GSBL
${password}=      Vanajarao10
${database}=      SNOW_TEST

*** Test Cases ***
Connect to Snowflake
    ${conn}=    snowflake.connector.connect    account=${account_url}    user=${user}    password=${password}    database=${database}
    Set Suite Variable    ${conn}

Run SQL Query in Snowflake
    ${query}=   Set Variable   SELECT * FROM siva
    ${conn}=    BuiltIn.Get Variables
    ${cursor}=  Evaluate  ${conn}.cursor()
    ${result}=  Evaluate  ${cursor}.execute('${query}')
    Log Many    ${result}

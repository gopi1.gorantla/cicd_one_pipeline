*** Settings ***
Library    snowflake.connector

*** Variables ***
${user}        GSBL
${password}    Vanajarao10
${account}     av29276.ap-southeast-1
${database}    SNOW_TEST
${warehouse}    SNOW_TEST
${schema}    SNOW_TEST
${connection}    # Define a variable to store the database connection

*** Keywords ***
Connect To Snowflake
    [Documentation]    Connect to a Snowflake database
    ${connection} =    Connect    user=${user}    password=${password}    account=${account}    database=${database}    warehouse=${warehouse}    schema=${schema}
    Run Keyword If    '${connection}' == 'None'    Fail    Connection to Snowflake failed: user=${user}, database=${database}, schema=${schema}
    [Return]    ${connection}

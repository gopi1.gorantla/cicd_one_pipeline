*** Settings ***
Resource    DatabaseKeywords.robot
Library    DatabaseQueries.py
Library    DatabaseLibrary

*** Variables ***
${sqlString}    select * from gsbl

*** Test Cases ***
Connect to Snowflake
    ${connection} =    Connect To Snowflake
    Log    Connection to Snowflake established: ${connection}

Run SQL Query in Snowflake
    ${results} =    Execute Snowflake Query    ${connection}    ${sqlString}
    Log    Query results: ${results}
#    Add further validation or assertions as needed

*** Keywords ***
Execute Snowflake Query
    [Arguments]    ${connection}    ${sqlString}
    ${result} =    Run Keyword    DatabaseLibrary.Execute Sql String    ${connection}    ${sqlString}
    [Return]    ${result}

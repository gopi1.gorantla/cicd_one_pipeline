import snowflake.connector

def execute_snowflake_query(connection, sqlString):
    # Execute a Snowflake query
    cursor = connection.cursor()
    try:
        # sqlString = "select * from siva"
        cursor.execute("select * from siva")
        result = cursor.fetchall()
    finally:
        cursor.close()
    return result



